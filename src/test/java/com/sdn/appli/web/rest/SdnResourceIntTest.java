package com.sdn.appli.web.rest;

import com.sdn.appli.AppliApp;

import com.sdn.appli.domain.Sdn;
import com.sdn.appli.repository.SdnRepository;
import com.sdn.appli.service.SdnService;
import com.sdn.appli.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.sdn.appli.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SdnResource REST controller.
 *
 * @see SdnResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppliApp.class)
public class SdnResourceIntTest {

    private static final String DEFAULT_SDN_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SDN_NAME = "BBBBBBBBBB";

    @Autowired
    private SdnRepository sdnRepository;

    

    @Autowired
    private SdnService sdnService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSdnMockMvc;

    private Sdn sdn;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SdnResource sdnResource = new SdnResource(sdnService);
        this.restSdnMockMvc = MockMvcBuilders.standaloneSetup(sdnResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sdn createEntity(EntityManager em) {
        Sdn sdn = new Sdn()
            .sdnName(DEFAULT_SDN_NAME);
        return sdn;
    }

    @Before
    public void initTest() {
        sdn = createEntity(em);
    }

    @Test
    @Transactional
    public void createSdn() throws Exception {
        int databaseSizeBeforeCreate = sdnRepository.findAll().size();

        // Create the Sdn
        restSdnMockMvc.perform(post("/api/sdns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sdn)))
            .andExpect(status().isCreated());

        // Validate the Sdn in the database
        List<Sdn> sdnList = sdnRepository.findAll();
        assertThat(sdnList).hasSize(databaseSizeBeforeCreate + 1);
        Sdn testSdn = sdnList.get(sdnList.size() - 1);
        assertThat(testSdn.getSdnName()).isEqualTo(DEFAULT_SDN_NAME);
    }

    @Test
    @Transactional
    public void createSdnWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sdnRepository.findAll().size();

        // Create the Sdn with an existing ID
        sdn.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSdnMockMvc.perform(post("/api/sdns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sdn)))
            .andExpect(status().isBadRequest());

        // Validate the Sdn in the database
        List<Sdn> sdnList = sdnRepository.findAll();
        assertThat(sdnList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSdns() throws Exception {
        // Initialize the database
        sdnRepository.saveAndFlush(sdn);

        // Get all the sdnList
        restSdnMockMvc.perform(get("/api/sdns?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sdn.getId().intValue())))
            .andExpect(jsonPath("$.[*].sdnName").value(hasItem(DEFAULT_SDN_NAME.toString())));
    }
    

    @Test
    @Transactional
    public void getSdn() throws Exception {
        // Initialize the database
        sdnRepository.saveAndFlush(sdn);

        // Get the sdn
        restSdnMockMvc.perform(get("/api/sdns/{id}", sdn.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sdn.getId().intValue()))
            .andExpect(jsonPath("$.sdnName").value(DEFAULT_SDN_NAME.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingSdn() throws Exception {
        // Get the sdn
        restSdnMockMvc.perform(get("/api/sdns/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSdn() throws Exception {
        // Initialize the database
        sdnService.save(sdn);

        int databaseSizeBeforeUpdate = sdnRepository.findAll().size();

        // Update the sdn
        Sdn updatedSdn = sdnRepository.findById(sdn.getId()).get();
        // Disconnect from session so that the updates on updatedSdn are not directly saved in db
        em.detach(updatedSdn);
        updatedSdn
            .sdnName(UPDATED_SDN_NAME);

        restSdnMockMvc.perform(put("/api/sdns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSdn)))
            .andExpect(status().isOk());

        // Validate the Sdn in the database
        List<Sdn> sdnList = sdnRepository.findAll();
        assertThat(sdnList).hasSize(databaseSizeBeforeUpdate);
        Sdn testSdn = sdnList.get(sdnList.size() - 1);
        assertThat(testSdn.getSdnName()).isEqualTo(UPDATED_SDN_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingSdn() throws Exception {
        int databaseSizeBeforeUpdate = sdnRepository.findAll().size();

        // Create the Sdn

        // If the entity doesn't have an ID, it will throw BadRequestAlertException 
        restSdnMockMvc.perform(put("/api/sdns")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sdn)))
            .andExpect(status().isBadRequest());

        // Validate the Sdn in the database
        List<Sdn> sdnList = sdnRepository.findAll();
        assertThat(sdnList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSdn() throws Exception {
        // Initialize the database
        sdnService.save(sdn);

        int databaseSizeBeforeDelete = sdnRepository.findAll().size();

        // Get the sdn
        restSdnMockMvc.perform(delete("/api/sdns/{id}", sdn.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Sdn> sdnList = sdnRepository.findAll();
        assertThat(sdnList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sdn.class);
        Sdn sdn1 = new Sdn();
        sdn1.setId(1L);
        Sdn sdn2 = new Sdn();
        sdn2.setId(sdn1.getId());
        assertThat(sdn1).isEqualTo(sdn2);
        sdn2.setId(2L);
        assertThat(sdn1).isNotEqualTo(sdn2);
        sdn1.setId(null);
        assertThat(sdn1).isNotEqualTo(sdn2);
    }
}
