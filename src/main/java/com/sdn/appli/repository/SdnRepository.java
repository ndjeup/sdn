package com.sdn.appli.repository;

import com.sdn.appli.domain.Sdn;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Sdn entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SdnRepository extends JpaRepository<Sdn, Long> {

}
