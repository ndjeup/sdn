package com.sdn.appli.service.impl;

import com.sdn.appli.service.SdnService;
import com.sdn.appli.domain.Sdn;
import com.sdn.appli.repository.SdnRepository;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;

/**
 * Service Implementation for managing Sdn.
 */
@Service
@Transactional
public class SdnServiceImpl implements SdnService {

	private final Logger log = LoggerFactory.getLogger(SdnServiceImpl.class);

	private final SdnRepository sdnRepository;

	public SdnServiceImpl(SdnRepository sdnRepository) {
		this.sdnRepository = sdnRepository;
	}

	/**
	 * Save a sdn.
	 *
	 * @param sdn
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public Sdn save(Sdn sdn) {
		log.debug("Request to save Sdn : {}", sdn);
		return sdnRepository.save(sdn);
	}

	/**
	 * Get all the sdns.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<Sdn> findAll(Pageable pageable) {
		log.debug("Request to get all Sdns");
		return sdnRepository.findAll(pageable);
	}

	/**
	 * Get one sdn by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public Optional<Sdn> findOne(Long id) {
		log.debug("Request to get Sdn : {}", id);
		return sdnRepository.findById(id);
	}

	/**
	 * Delete the sdn by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete Sdn : {}", id);
		sdnRepository.deleteById(id);
	}
	
	public void ftpDownloadFile(String serverAddress, String userName, String password, String remoteFilePath,
			String localFile, int port) {
		log.debug("Executing ftpDownloadFile");
		FTPClient ftpClient = new FTPClient();
		try {

			ftpClient.connect(serverAddress, port);
			ftpClient.login(userName, password);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			File localfile = new File("E:/ftpServerFile.txt");
			FileOutputStream outputStream = new FileOutputStream(localfile);
			boolean success = ftpClient.retrieveFile(remoteFilePath, outputStream);
			outputStream.close();

			if (success) {
				System.out.println("Ftp file successfully download.");
			}

		} catch (IOException ex) {
			System.out.println("Error occurs in downloading files from ftp Server : " + ex.getMessage());
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
