package com.sdn.appli.service.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/**
 * 
 * Utility Class to get data from URL Rest API
 *
 */
public class JsonUtil {

	@SuppressWarnings("rawtypes")
	public Map<String, Comparable> getQuote(String apiUrl) {
		HttpURLConnection connection = null;
		String inputLine;
		Map<String, Comparable> quote = new HashMap<String, Comparable>();
		try {
			URL url = new URL(apiUrl);
			// Make connection
			connection = (HttpURLConnection) url.openConnection();
			// Set request type as HTTP GET
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			String staging = "";
			if (connection.getResponseCode() == 200) {
				// get response stream
				BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				// feed response into the StringBuilder
				while ((inputLine = input.readLine()) != null) {
					staging = inputLine.replaceAll("\\[", "");
					staging = staging.replaceAll("\\]", "");
				}
				input.close();
				JSONObject objJson = new JSONObject(staging.toString());
				quote.put("price", objJson.getDouble("price"));
				quote.put("bid", objJson.getDouble("bid"));
				quote.put("symbol", objJson.getString("symbol"));
				quote.put("ask", objJson.getDouble("ask"));
				quote.put("timestamp", objJson.getInt("timestamp"));

			} else {
				System.out.println("Can't get data");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return quote;
	}
		public static void main(String[] args) {
			String targetURL = "https://forex.1forge.com/1.0.3/quotes?pairs=CADUSD&api_key=cFqljy1OkQFsWQGUa7gNSOwbJ3YWefky";
			JsonUtil json = new JsonUtil();
			System.out.println(json.getQuote(targetURL).get("ask"));

	}
}
