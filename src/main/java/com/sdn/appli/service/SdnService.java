package com.sdn.appli.service;

import com.sdn.appli.domain.Sdn;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Sdn.
 */
public interface SdnService {

    /**
     * Save a sdn.
     *
     * @param sdn the entity to save
     * @return the persisted entity
     */
    Sdn save(Sdn sdn);

    /**
     * Get all the sdns.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Sdn> findAll(Pageable pageable);


    /**
     * Get the "id" sdn.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Sdn> findOne(Long id);

    /**
     * Delete the "id" sdn.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
