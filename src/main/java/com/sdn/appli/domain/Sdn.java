package com.sdn.appli.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Sdn.
 */
@Entity
@Table(name = "tb_sdnlist")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Sdn implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sdn_name")
    private String sdnName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSdnName() {
        return sdnName;
    }

    public Sdn sdnName(String sdnName) {
        this.sdnName = sdnName;
        return this;
    }

    public void setSdnName(String sdnName) {
        this.sdnName = sdnName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sdn sdn = (Sdn) o;
        if (sdn.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sdn.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Sdn{" +
            "id=" + getId() +
            ", sdnName='" + getSdnName() + "'" +
            "}";
    }
}
