package com.sdn.appli.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sdn.appli.domain.Sdn;
import com.sdn.appli.service.SdnService;
import com.sdn.appli.web.rest.errors.BadRequestAlertException;
import com.sdn.appli.web.rest.util.HeaderUtil;
import com.sdn.appli.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Sdn.
 */
@RestController
@RequestMapping("/api")
public class SdnResource {

    private final Logger log = LoggerFactory.getLogger(SdnResource.class);

    private static final String ENTITY_NAME = "sdn";

    private final SdnService sdnService;

    public SdnResource(SdnService sdnService) {
        this.sdnService = sdnService;
    }

    /**
     * POST  /sdns : Create a new sdn.
     *
     * @param sdn the sdn to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sdn, or with status 400 (Bad Request) if the sdn has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sdns")
    @Timed
    public ResponseEntity<Sdn> createSdn(@RequestBody Sdn sdn) throws URISyntaxException {
        log.debug("REST request to save Sdn : {}", sdn);
        if (sdn.getId() != null) {
            throw new BadRequestAlertException("A new sdn cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Sdn result = sdnService.save(sdn);
        return ResponseEntity.created(new URI("/api/sdns/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sdns : Updates an existing sdn.
     *
     * @param sdn the sdn to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sdn,
     * or with status 400 (Bad Request) if the sdn is not valid,
     * or with status 500 (Internal Server Error) if the sdn couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sdns")
    @Timed
    public ResponseEntity<Sdn> updateSdn(@RequestBody Sdn sdn) throws URISyntaxException {
        log.debug("REST request to update Sdn : {}", sdn);
        if (sdn.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Sdn result = sdnService.save(sdn);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sdn.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sdns : get all the sdns.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of sdns in body
     */
    @GetMapping("/sdns")
    @Timed
    public ResponseEntity<List<Sdn>> getAllSdns(Pageable pageable) {
        log.debug("REST request to get a page of Sdns");
        Page<Sdn> page = sdnService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sdns");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sdns/:id : get the "id" sdn.
     *
     * @param id the id of the sdn to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sdn, or with status 404 (Not Found)
     */
    @GetMapping("/sdns/{id}")
    @Timed
    public ResponseEntity<Sdn> getSdn(@PathVariable Long id) {
        log.debug("REST request to get Sdn : {}", id);
        Optional<Sdn> sdn = sdnService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sdn);
    }

    /**
     * DELETE  /sdns/:id : delete the "id" sdn.
     *
     * @param id the id of the sdn to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sdns/{id}")
    @Timed
    public ResponseEntity<Void> deleteSdn(@PathVariable Long id) {
        log.debug("REST request to delete Sdn : {}", id);
        sdnService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
