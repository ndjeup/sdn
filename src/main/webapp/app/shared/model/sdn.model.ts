export interface ISdn {
  id?: number;
  sdnName?: string;
}

export const defaultValue: Readonly<ISdn> = {};
