import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Sdn from './sdn';
import SdnDetail from './sdn-detail';
import SdnUpdate from './sdn-update';
import SdnDeleteDialog from './sdn-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={SdnUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={SdnUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={SdnDetail} />
      <ErrorBoundaryRoute path={match.url} component={Sdn} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={SdnDeleteDialog} />
  </>
);

export default Routes;
